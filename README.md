CAS Account Link
================

Description
-----------

Allows a user to link its CAS account with its Drupal account on sites where the
CAS setting `user_accounts.auto_register` is _on_. After a first successful CAS
login, the users will see a form where they are able to make a decision:
 
* Either allow Drupal to create a new local account;
* Or login with the local credentials and link the CAS account with the local
  account.

Requirements
------------
  
* [CAS](https://www.drupal.org/project/cas) module.
* [CAS mock server](https://www.drupal.org/project/cas_mock_server) is
  dependency for module tests.

Configuration
-------------

* Go to `/admin/config/people/cas` and turn on the _Automatically register
  users_ checkbox. Save the configuration.
* In order to customize the form provided by the module, visit
  `/admin/config/people/cas/account-link` and customize various texts that are
  exposed on the form.
* A more advanced form customization could be made by developers by altering the
  form.
