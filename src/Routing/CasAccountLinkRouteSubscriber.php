<?php

declare(strict_types = 1);

namespace Drupal\cas_account_link\Routing;

use Drupal\cas_account_link\Access\CasAccountLinkAccess;
use Drupal\cas_account_link\Form\CasAccountLinkForm;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Provides a route subscriber.
 */
class CasAccountLinkRouteSubscriber extends RouteSubscriberBase {

  /**
   * The config factory interface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new route subscriber instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory interface.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    $settings = $this->configFactory->get('cas_account_link.settings');
    $collection->add('cas_account_link.form', (new Route($settings->get('form.path')))
      ->setDefault('_form', CasAccountLinkForm::class)
      ->setDefault('_title', $settings->get('form.title'))
      ->setRequirement('_custom_access', CasAccountLinkAccess::class . '::access'));
  }

}
