<?php

declare(strict_types = 1);

namespace Drupal\cas_account_link\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Custom access handler for cas_account_link.form route.
 */
class CasAccountLinkAccess implements ContainerInjectionInterface {

  /**
   * The CAS settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $casSettings;

  /**
   * The private tempstore.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $privateTempStore;

  /**
   * Construct a new access handler instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $private_tempstore_factory
   *   The private temp store factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, PrivateTempStoreFactory $private_tempstore_factory) {
    $this->casSettings = $config_factory->get('cas.settings');
    $this->privateTempStore = $private_tempstore_factory->get('cas_account_link');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('config.factory'),
      $container->get('tempstore.private')
    );
  }

  /**
   * Checks the access to cas_account_link.form route.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result object.
   */
  public function access(AccountInterface $account): AccessResultInterface {
    // Only anonymous users can access this route.
    if ($account->isAuthenticated()) {
      return AccessResult::forbidden('Authenticated user');
    }

    // The form makes sense only with user account auto registering.
    if (!$this->casSettings->get('user_accounts.auto_register')) {
      return AccessResult::forbidden('user_accounts.auto_register is FALSE');
    }

    // Forbid access if there's no context created upstream, after a successful
    // CAS authentication and ticket validation.
    $context_keys = ['ticket', 'property_bag', 'service_parameters'];

    foreach ($context_keys as $context_key) {
      if (($value = $this->privateTempStore->get($context_key)) === NULL) {
        return AccessResult::forbidden('No CAS context');
      }
      if ($context_key === 'ticket' && !$value) {
        return AccessResult::forbidden('No CAS ticket');
      }
    }

    return AccessResult::allowed();
  }

}
