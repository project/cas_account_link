<?php

declare(strict_types = 1);

namespace Drupal\cas_account_link\Event\Subscriber;

use Drupal\cas\Event\CasPreRegisterEvent;
use Drupal\cas\Event\CasPreUserLoadRedirectEvent;
use Drupal\cas\Service\CasHelper;
use Drupal\Component\Utility\Random;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\externalauth\ExternalAuthInterface;
use Drupal\user\UserInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Listens to CAS events.
 */
class CasAccountLinkSubscriber implements EventSubscriberInterface {

  /**
   * The URL generator service.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * The CAS settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $casSettings;

  /**
   * The external authentication service.
   *
   * @var \Drupal\externalauth\ExternalAuthInterface
   */
  protected $externalAuth;

  /**
   * The private tempstore.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $privateTempstore;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * Constructs a new event subscriber instance.
   *
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   The URL generator service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\externalauth\ExternalAuthInterface $external_auth
   *   The external authentication service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $private_tempstore_factory
   *   The private tempstore factory service.
   * @param \Drupal\Core\Http\RequestStack $request_stack
   *   The current request.
   */
  public function __construct(UrlGeneratorInterface $url_generator, ConfigFactoryInterface $config_factory, ExternalAuthInterface $external_auth, PrivateTempStoreFactory $private_tempstore_factory, RequestStack $request_stack) {
    $this->urlGenerator = $url_generator;
    $this->casSettings = $config_factory->get('cas.settings');
    $this->externalAuth = $external_auth;
    $this->privateTempstore = $private_tempstore_factory->get('cas_account_link');
    $this->currentRequest = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      CasHelper::EVENT_PRE_USER_LOAD_REDIRECT => 'onRedirect',
      CasHelper::EVENT_PRE_REGISTER => 'handleDrupalUsernameCollision',
    ];
  }

  /**
   * Listens to CasHelper::EVENT_PRE_USER_LOAD_REDIRECT event.
   *
   * @param \Drupal\cas\Event\CasPreUserLoadRedirectEvent $event
   *   The pre user load event object.
   */
  public function onRedirect(CasPreUserLoadRedirectEvent $event): void {
    // Create the redirect only if CAS is configured with user account auto
    // registration and the user doesn't have a local account.
    if ($this->casSettings->get('user_accounts.auto_register') && !$this->externalAuth->load($event->getPropertyBag()->getUsername(), 'cas')) {
      // Store relevant data required by the CAS Account Link form.
      $this->privateTempstore->set('ticket', $event->getTicket());
      $this->privateTempstore->set('property_bag', $event->getPropertyBag());
      $this->privateTempstore->set('service_parameters', $event->getServiceParameters());

      $url = $this->urlGenerator->generateFromRoute('cas_account_link.form');
      // Remove the 'destination' query parameter from the request, otherwise
      // we'll redirect there instead of form. The value is not lost, as it has
      // been just saved in the private tempstore.
      $this->currentRequest->query->remove('destination');
      $event->setRedirectResponse(new RedirectResponse($url));
    }
  }

  /**
   * Assigns a new name to the user being registered if a local user exists.
   *
   * @param \Drupal\cas\Event\CasPreRegisterEvent $event
   *   The CAS pre user register event object.
   */
  public function handleDrupalUsernameCollision(CasPreRegisterEvent $event): void {
    if (!$this->casSettings->get('user_accounts.auto_register')) {
      return;
    }

    $new_name = $name = $event->getDrupalUsername();
    while (user_load_by_name($new_name)) {
      $new_name = $this->buildUserName($name);
    }

    $event->setDrupalUsername($new_name);
  }

  /**
   * Builds a new username starting from the original name.
   *
   * @param string $name
   *   The original name passed by the event dispatcher.
   *
   * @return string
   *   A new name that pretends to be unique.
   */
  protected function buildUserName(string $name): string {
    if (mb_strlen($name) > UserInterface::USERNAME_MAX_LENGTH - 7) {
      // In case the original name is too long, make some room for the suffix.
      $name = substr($name, 0, UserInterface::USERNAME_MAX_LENGTH - 7);
    }
    return $name . '_' . (new Random())->name(6, TRUE);
  }

}
