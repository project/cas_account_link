<?php

declare(strict_types = 1);

namespace Drupal\cas_account_link\Event\Events;

use Drupal\cas\CasPropertyBag;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Provides an event class for CasAccountLinkEvents::EMAIL_COLLISION events.
 */
class CasAccountLinkEmailCollisionEvent extends Event {

  /**
   * The proposed local account Email.
   *
   * @var string
   */
  protected $localMail;

  /**
   * The CAS property bag.
   *
   * @var \Drupal\cas\CasPropertyBag
   */
  protected $casPropertyBag;

  /**
   * The translated error message as a markup object or as a render array.
   *
   * @var \Drupal\Component\Render\MarkupInterface|array
   */
  protected $errorMessage;

  /**
   * Constructs a new event object.
   *
   * @param string $local_mail
   *   The proposed local email.
   * @param \Drupal\cas\CasPropertyBag $cas_property_bag
   *   The CAS property bag.
   */
  public function __construct(string $local_mail, CasPropertyBag $cas_property_bag) {
    $this->localMail = $local_mail;
    $this->casPropertyBag = $cas_property_bag;
  }

  /**
   * Returns the local email.
   *
   * @return string
   *   The local mail.
   */
  public function getLocalMail(): string {
    return $this->localMail;
  }

  /**
   * Returns the CAS property bag.
   *
   * @return \Drupal\cas\CasPropertyBag
   *   The CAS property bag.
   */
  public function getCasPropertyBag(): CasPropertyBag {
    return $this->casPropertyBag;
  }

  /**
   * Sets the error message as a translatable markup object.
   *
   * @param \Drupal\Component\Render\MarkupInterface|array $error_message
   *   The translated error message as a translated markup or as a render array.
   *
   * @return $this
   */
  public function setErrorMessage($error_message): self {
    $this->errorMessage = $error_message;
    return $this;
  }

  /**
   * Returns the translated error message.
   *
   * @return \Drupal\Component\Render\MarkupInterface|array|null
   *   A translated text as a markup object or a render array.
   */
  public function getErrorMessage() {
    return $this->errorMessage;
  }

}
