<?php

declare(strict_types = 1);

namespace Drupal\cas_account_link\Event\Events;

use Drupal\Core\Form\FormStateInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Provides an event class for CasAccountLinkEvents::VALIDATE events.
 */
class CasAccountLinkValidateEvent extends Event {

  /**
   * The form to be validated.
   *
   * @var array
   */
  protected $form;

  /**
   * The form state object.
   *
   * @var \Drupal\Core\Form\FormStateInterface
   */
  protected $formState;

  /**
   * Constructs a new event object.
   *
   * @param array $form
   *   The form to be validated.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public function __construct(array $form, FormStateInterface $form_state) {
    $this->form = $form;
    $this->formState = $form_state;
  }

  /**
   * Returns the form.
   *
   * @return array
   *   The form to be validated.
   */
  public function getForm(): array {
    return $this->form;
  }

  /**
   * Sets the form render array.
   *
   * @param array $form
   *   The form render array.
   *
   * @return $this
   */
  public function setForm(array $form): self {
    $this->form = $form;
    return $this;
  }

  /**
   * Returns the form state object.
   *
   * @return \Drupal\Core\Form\FormStateInterface
   *   The the form state object.
   */
  public function getFormState(): FormStateInterface {
    return $this->formState;
  }

}
