<?php

declare(strict_types = 1);

namespace Drupal\cas_account_link\Event\Events;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Url;
use Drupal\cas\CasPropertyBag;
use Drupal\user\UserInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Provides an event class for CasAccountLinkEvents::POST_LINK events.
 */
class CasAccountLinkPostLinkEvent extends Event {

  /**
   * The CAS ticket.
   *
   * @var string
   */
  protected $casTicket;

  /**
   * The CAS property bag.
   *
   * @var \Drupal\cas\CasPropertyBag
   */
  protected $casPropertyBag;

  /**
   * The CAS service parameters.
   *
   * @var array
   */
  protected $casServiceParameters;

  /**
   * The local user account.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $account;

  /**
   * The user selection: TRUE for 'yes', FALSE for 'no'.
   *
   * @var bool
   */
  protected $localAccountSelected;

  /**
   * The translated success message.
   *
   * @var \Drupal\Component\Render\MarkupInterface
   */
  protected $successMessage;

  /**
   * The form redirect URL.
   *
   * @var \Drupal\Core\Url
   */
  protected $redirectUrl;

  /**
   * Constructs a new event object.
   *
   * @param string $cas_ticket
   *   The CAS ticket.
   * @param \Drupal\cas\CasPropertyBag $cas_property_bag
   *   The CAS property bag.
   * @param array $cas_service_parameters
   *   The CAS service parameters.
   * @param \Drupal\user\UserInterface $account
   *   The local user account.
   * @param bool $local_account_selected
   *   The user selection: TRUE for 'yes', FALSE for 'no'.
   */
  public function __construct(string $cas_ticket, CasPropertyBag $cas_property_bag, array $cas_service_parameters, UserInterface $account, bool $local_account_selected) {
    $this->casTicket = $cas_ticket;
    $this->casPropertyBag = $cas_property_bag;
    $this->casServiceParameters = $cas_service_parameters;
    $this->account = $account;
    $this->localAccountSelected = $local_account_selected;
  }

  /**
   * Returns the CAS ticket.
   *
   * @return string
   *   The CAS ticket.
   */
  public function getCasTicket(): string {
    return $this->casTicket;
  }

  /**
   * Returns the CAS property bag.
   *
   * @return \Drupal\cas\CasPropertyBag
   *   The CAS property bag.
   */
  public function getCasPropertyBag(): CasPropertyBag {
    return $this->casPropertyBag;
  }

  /**
   * Returns the CAS service parameters.
   *
   * @return array
   *   The CAS service parameters.
   */
  public function getCasServiceParameters(): array {
    return $this->casServiceParameters;
  }

  /**
   * Returns the local user account.
   *
   * @return \Drupal\user\UserInterface
   *   The local user account.
   */
  public function getAccount(): UserInterface {
    return $this->account;
  }

  /**
   * Returns if user selected that they have a local account.
   *
   * @return bool
   *   The user selection: TRUE for 'yes', FALSE for 'no'.
   */
  public function isLocalAccountSelected(): bool {
    return $this->localAccountSelected;
  }

  /**
   * Sets the success message as a translatable markup object.
   *
   * @param \Drupal\Component\Render\MarkupInterface $success_message
   *   The translated success message.
   *
   * @return $this
   */
  public function setSuccessMessage(MarkupInterface $success_message): self {
    $this->successMessage = $success_message;
    return $this;
  }

  /**
   * Returns the translated success message.
   *
   * @return \Drupal\Component\Render\MarkupInterface|null
   *   The translated success message.
   */
  public function getSuccessMessage(): ?MarkupInterface {
    return $this->successMessage;
  }

  /**
   * Sets the form redirect URL.
   *
   * @param \Drupal\Core\Url $redirect_url
   *   The form redirect URL.
   *
   * @return $this
   */
  public function setRedirectUrl(Url $redirect_url): self {
    $this->redirectUrl = $redirect_url;
    return $this;
  }

  /**
   * Returns the form redirect URL.
   *
   * @return \Drupal\Core\Url|null
   *   The form redirect URL.
   */
  public function getRedirectUrl(): ?Url {
    return $this->redirectUrl;
  }

}
