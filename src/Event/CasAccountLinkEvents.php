<?php

namespace Drupal\cas_account_link\Event;

/**
 * Provides CAS Account Link events identifiers.
 */
final class CasAccountLinkEvents {

  /**
   * Event identifier for the CasAccountLinkPostLinkEvent event.
   *
   * @var string
   */
  const POST_LINK = 'cas_account_link.post_link';

  /**
   * Event identifier for the CasAccountLinkEmailCollisionEvent event.
   *
   * @var string
   */
  const EMAIL_COLLISION = 'cas_account_link.email_collision';

  /**
   * Event identifier for the CasAccountLinkValidateEvent event.
   *
   * @var string
   */
  const VALIDATE = 'cas_account_link.validate';

}
