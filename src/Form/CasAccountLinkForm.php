<?php

declare(strict_types = 1);

namespace Drupal\cas_account_link\Form;

use Drupal\cas\Service\CasHelper;
use Drupal\cas\Service\CasUserManager;
use Drupal\cas_account_link\Event\CasAccountLinkEvents;
use Drupal\cas_account_link\Event\Events\CasAccountLinkEmailCollisionEvent;
use Drupal\cas_account_link\Event\Events\CasAccountLinkPostLinkEvent;
use Drupal\cas_account_link\Event\Events\CasAccountLinkValidateEvent;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Drupal\externalauth\ExternalAuthInterface;
use Drupal\user\Entity\User;
use Drupal\user\Form\UserLoginForm;
use Psr\Log\LogLevel;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Provides a form that allows the user to link its CAS and Drupal accounts.
 */
class CasAccountLinkForm extends FormBase {

  /**
   * The private temp store.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $privateTempStore;

  /**
   * The CAS user manager service.
   *
   * @var \Drupal\cas\Service\CasUserManager
   */
  protected $casUserManager;

  /**
   * The CAS helper service.
   *
   * @var \Drupal\cas\Service\CasHelper
   */
  protected $casHelper;

  /**
   * The class resolver service.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * The external auth service.
   *
   * @var \Drupal\externalauth\ExternalAuthInterface
   */
  protected $externalAuth;

  /**
   * The session service.
   *
   * @var \Symfony\Component\HttpFoundation\Session\SessionInterface
   */
  protected $session;

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The user login form instance.
   *
   * @var \Drupal\user\Form\UserLoginForm
   */
  protected $loginForm;

  /**
   * The user selection.
   *
   * @var bool
   */
  protected $localAccountSelected;

  /**
   * Constructs a new form instance.
   *
   * @param \Drupal\cas\Service\CasUserManager $cas_user_manager
   *   The CAS user manager service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $private_tempstore_factory
   *   The private temp store factory service.
   * @param \Drupal\cas\Service\CasHelper $cas_helper
   *   The CAS helper service.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The class resolver service.
   * @param \Drupal\externalauth\ExternalAuthInterface $external_auth
   *   The external auth service.
   * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session
   *   The session service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher service.
   */
  public function __construct(CasUserManager $cas_user_manager, PrivateTempStoreFactory $private_tempstore_factory, CasHelper $cas_helper, ClassResolverInterface $class_resolver, ExternalAuthInterface $external_auth, SessionInterface $session, EventDispatcherInterface $event_dispatcher) {
    $this->casUserManager = $cas_user_manager;
    $this->privateTempStore = $private_tempstore_factory->get('cas_account_link');
    $this->casHelper = $cas_helper;
    $this->classResolver = $class_resolver;
    $this->externalAuth = $external_auth;
    $this->session = $session;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cas.user_manager'),
      $container->get('tempstore.private'),
      $container->get('cas.helper'),
      $container->get('class_resolver'),
      $container->get('externalauth.externalauth'),
      $container->get('session'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $settings = $this->config('cas_account_link.settings');

    if ($settings->get('form.help.value')) {
      $form['help'] = [
        '#type' => 'processed_text',
        '#text' => $settings->get('form.help.value'),
        '#format' => $settings->get('form.help.format'),
      ];
    }

    $form['account_exist'] = [
      '#type' => 'radios',
      '#title' => $settings->get('form.question.title'),
      '#description' => $settings->get('form.question.description'),
      '#options' => [
        'yes' => $settings->get('form.question.answers.yes.label'),
        'no' => $settings->get('form.question.answers.no.label'),
      ],
      'yes' => [
        '#description' => $settings->get('form.question.answers.yes.description'),
      ],
      'no' => [
        '#description' => $settings->get('form.question.answers.no.description'),
      ],
      '#required' => TRUE,
    ];

    $form['login'] = [];
    $subform_state = SubformState::createForSubform($form['login'], $form, $form_state);
    $form['login'] = $this->getLoginForm()->buildForm($form['login'], $subform_state);
    $form['login']['#type'] = 'container';
    $form['login']['#tree'] = TRUE;
    $form['login']['name']['#required'] = FALSE;
    $form['login']['pass']['#required'] = FALSE;
    $form['login']['#states'] = [
      'visible' => [
        ':input[name="account_exist"]' => ['value' => 'yes'],
      ],
    ];
    $form['login']['actions']['submit']['#value'] = $settings->get('form.question.answers.yes.button');

    // In order to expose different button labels and avoid adding a Javascript
    // snippet, we place two buttons and use the form element `#states`
    // functionality to show/hide them based on the user selection. However, the
    // login form has its own `#actions` container and we need to move the
    // submit button in the main form actions.
    $submit_login = $form['login']['actions']['submit'];
    unset($form['login']['actions']);
    $submit_login['#value'] = $settings->get('form.question.answers.yes.button');
    $submit_login['#states'] = [
      'visible' => [
        ':input[name="account_exist"]' => ['value' => 'yes'],
      ],
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit_login' => $submit_login,
      'submit_register' => [
        '#type' => 'submit',
        '#value' => $settings->get('form.question.answers.no.button'),
        '#states' => [
          'visible' => [
            ':input[name="account_exist"]' => ['value' => 'no'],
          ],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    if ($form_state->getValue('account_exist') === 'no') {
      /** @var \Drupal\cas\CasPropertyBag $property_bag */
      $property_bag = $this->privateTempStore->get('property_bag');
      $local_mail = $this->casUserManager->getEmailForNewAccount($property_bag);
      // Email collision.
      if (user_load_by_mail($local_mail)) {
        $email_collision_event = new CasAccountLinkEmailCollisionEvent($local_mail, $property_bag);
        $this->casHelper->log(LogLevel::DEBUG, 'Dispatching CasAccountLinkEvents::EMAIL_COLLISION.');
        $this->eventDispatcher->dispatch($email_collision_event, CasAccountLinkEvents::EMAIL_COLLISION);
        if (!$message = $email_collision_event->getErrorMessage()) {
          $message = $this->t('The email address %mail is already taken.', [
            '%mail' => $local_mail,
          ]);
        }
        $form_state->setErrorByName('account_exist', $message);
      }
      return;
    }

    // We've been disabled the core required validation. Do it here.
    if ($form_state->isValueEmpty(['login', 'name'])) {
      $form_state->setErrorByName('login][name', $this->t('Username field is required.'));
    }
    if ($form_state->isValueEmpty(['login', 'pass'])) {
      $form_state->setErrorByName('login][pass', $this->t('Password field is required.'));
    }

    // There's no way to call all sub-form validators once. Each sub-form
    // validator should be called explicitly.
    // @see https://www.drupal.org/project/drupal/issues/3065316
    $subform_state = SubformState::createForSubform($form['login'], $form, $form_state);
    $this->getLoginForm()->validateName($form['login'], $subform_state);
    $this->getLoginForm()->validateAuthentication($form['login'], $subform_state);
    $this->getLoginForm()->validateFinal($form['login'], $subform_state);

    // If errors were already set, exit here, as login errors are critical and
    // any future validations need a valid user.
    if ($form_state->getErrors()) {
      return;
    }

    // Let third party add their own validation.
    $this->casHelper->log(LogLevel::DEBUG, 'Dispatching CasAccountLinkEvents::VALIDATE');
    $event = new CasAccountLinkValidateEvent($form, $form_state);
    $this->eventDispatcher->dispatch($event, CasAccountLinkEvents::VALIDATE);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    /** @var string $ticket */
    $ticket = $this->privateTempStore->get('ticket');
    /** @var \Drupal\cas\CasPropertyBag $property_bag */
    $property_bag = $this->privateTempStore->get('property_bag');
    /** @var array $service_parameters */
    $service_parameters = $this->privateTempStore->get('service_parameters');
    $this->privateTempStore->delete('ticket');
    $this->privateTempStore->delete('property_bag');
    $this->privateTempStore->delete('service_parameters');
    $is_local_account_selected = $form_state->getValue('account_exist') === 'yes';

    $account = $this->externalAuth->load($property_bag->getUsername(), 'cas');

    // This is, theoretically, impossible as we cannot be redirected here if a
    // linked local account exists. However, it's possible that after showing
    // the form and just before submitting, the local user gets created, either
    // by the user, using other session, or as an effect of other UI/API
    // concurrent process.
    if ($account) {
      // Just log the event.
      $this->casHelper->log(LogLevel::DEBUG, 'While trying to manually link its CAS with its local account, another process has already linked CAS @authname with local @username.', [
        '@authname' => $property_bag->getUsername(),
        '@username' => $account->getAccountName(),
      ]);
    }
    // Link the existing account.
    elseif ($is_local_account_selected) {
      /** @var \Drupal\user\UserInterface $account */
      $account = User::load($form_state->get('uid'));
      $authname = $property_bag->getUsername();
      $this->externalAuth->linkExistingAccount($authname, 'cas', $account);
      $this->casHelper->log(LogLevel::DEBUG, 'Linked the CAS account @authname with the local account @account.', [
        '@authname' => $authname,
        '@account' => $account->getAccountName(),
      ]);
    }

    // Do the actual login and, if necessary, the user registration.
    $this->casUserManager->login($property_bag, $ticket);

    // Add the service parameters to the request query string.
    $this->getRequest()->query->add($service_parameters);

    // At this point we know that the local account exists, let's load it.
    if (!$account) {
      $account = $this->externalAuth->load($property_bag->getUsername(), 'cas');
    }
    // Make the account entity available for other subsequent submit callbacks.
    $form_state->setValue('account', $account);

    // Allow third-party modules to decide how to end. They can set their
    // redirect and/or their success status message.
    $this->casHelper->log(LogLevel::DEBUG, 'Dispatching CasAccountLinkEvents::POST_LINK');
    $event = new CasAccountLinkPostLinkEvent($ticket, $property_bag, $service_parameters, $account, $is_local_account_selected);
    $this->eventDispatcher->dispatch($event, CasAccountLinkEvents::POST_LINK);

    // Third-party modules success message has precedence.
    if (!$success_message = $event->getSuccessMessage()) {
      // Fallback to default success message, if exists.
      $success_message = $this->config('cas.settings')->get('login_success_message');
    }
    // Inform the user about the successful login.
    if ($success_message) {
      $this->messenger()->addStatus($success_message);
    }

    // Third-party modules HTTP redirect response has precedence.
    if ($redirect_url = $event->getRedirectUrl()) {
      // If a subscriber responded with a redirect URL, we should not enforce
      // a redirection to a potential query 'destination' parameter.
      $this->getRequest()->query->remove('destination');
    }
    else {
      $this->casHelper->handleReturnToParameter($this->getRequest());
      $redirect_url = Url::fromRoute('<front>');
    }
    $form_state->setRedirectUrl($redirect_url);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'cas_account_link_form';
  }

  /**
   * Returns the user login form instance.
   *
   * @return \Drupal\user\Form\UserLoginForm
   *   The user login form instance.
   */
  protected function getLoginForm(): UserLoginForm {
    if (!isset($this->loginForm)) {
      $this->loginForm = $this->classResolver->getInstanceFromDefinition(UserLoginForm::class);
    }
    return $this->loginForm;
  }

}
