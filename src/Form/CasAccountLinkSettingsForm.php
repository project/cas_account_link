<?php

declare(strict_types = 1);

namespace Drupal\cas_account_link\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a settings form for CAS Account Link module.
 */
class CasAccountLinkSettingsForm extends ConfigFormBase {

  /**
   * The route builder service.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routeBuilder;

  /**
   * Constructs a new form instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Routing\RouteBuilderInterface $route_builder
   *   The route builder service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RouteBuilderInterface $route_builder) {
    parent::__construct($config_factory);
    $this->routeBuilder = $route_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static($container->get('config.factory'), $container->get('router.builder'));
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $settings = $this->config('cas_account_link.settings');

    $form['form'] = [
      '#tree' => TRUE,
    ];
    $form['form']['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The path of the form page'),
      '#default_value' => $settings->get('form.path'),
      '#required' => TRUE,
    ];
    $form['form']['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The title of the form page'),
      '#default_value' => $settings->get('form.title'),
      '#required' => TRUE,
    ];
    $form['form']['help'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Optional help text to be displayed on top of the form'),
      '#default_value' => $settings->get('form.help.value'),
      '#format' => $settings->get('form.help.format'),
      '#editor' => TRUE,
    ];
    $form['form']['question']['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The question text'),
      '#default_value' => $settings->get('form.question.title'),
      '#required' => TRUE,
    ];
    $form['form']['question']['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('The question description'),
      '#default_value' => $settings->get('form.question.description'),
    ];
    $form['form']['question']['answers']['yes']['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t("The answer label if the account exists"),
      '#default_value' => $settings->get('form.question.answers.yes.label'),
      '#required' => TRUE,
    ];
    $form['form']['question']['answers']['yes']['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t("The answer description if the account exists"),
      '#default_value' => $settings->get('form.question.answers.yes.description'),
    ];
    $form['form']['question']['answers']['yes']['button'] = [
      '#type' => 'textfield',
      '#title' => $this->t("The form submit button label if the account exists"),
      '#default_value' => $settings->get('form.question.answers.yes.button'),
      '#required' => TRUE,
    ];
    $form['form']['question']['answers']['no']['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t("The answer label if the account doesn't exist"),
      '#default_value' => $settings->get('form.question.answers.no.label'),
      '#required' => TRUE,
    ];
    $form['form']['question']['answers']['no']['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t("The answer description if the account doesn't exist"),
      '#default_value' => $settings->get('form.question.answers.no.description'),
    ];
    $form['form']['question']['answers']['no']['button'] = [
      '#type' => 'textfield',
      '#title' => $this->t("The form submit button label if the account doesn't exist"),
      '#default_value' => $settings->get('form.question.answers.no.button'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    $settings = $this->config('cas_account_link.settings');
    $original_form_path = $settings->get('form.path');
    $settings->set('form', $form_state->getValue('form'))->save();

    // If the form path has been changed, rebuild the routes.
    if ($original_form_path !== $settings->get('form.path')) {
      $this->routeBuilder->setRebuildNeeded();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames(): array {
    return ['cas_account_link.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'cas_account_link_settings';
  }

}
