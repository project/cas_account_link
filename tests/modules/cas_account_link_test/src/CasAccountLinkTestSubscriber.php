<?php

declare(strict_types = 1);

namespace Drupal\cas_account_link_test;

use Drupal\cas_account_link\Event\CasAccountLinkEvents;
use Drupal\cas_account_link\Event\Events\CasAccountLinkPostLinkEvent;
use Drupal\cas_account_link\Event\Events\CasAccountLinkValidateEvent;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Testing subscriber.
 */
class CasAccountLinkTestSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a CasAccountLinkTestSubscriber object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      CasAccountLinkEvents::POST_LINK => 'onPostLink',
      CasAccountLinkEvents::VALIDATE => 'onValidate',
    ];
  }

  /**
   * Listens to CasAccountLinkEvents::POST_LINK event.
   *
   * @param \Drupal\cas_account_link\Event\Events\CasAccountLinkPostLinkEvent $event
   *   The event object.
   */
  public function onPostLink(CasAccountLinkPostLinkEvent $event): void {
    if ($this->state->get('cas_account_link_test.use_custom', FALSE)) {
      $event->setSuccessMessage($this->t('Custom success message'));
      $event->setRedirectUrl($event->getAccount()->toUrl('edit-form'));
    }
  }

  /**
   * Listens to CasAccountLinkEvents::VALIDATE event.
   *
   * @param \Drupal\cas_account_link\Event\Events\CasAccountLinkValidateEvent $event
   *   The event object.
   */
  public function onValidate(CasAccountLinkValidateEvent $event): void {
    if ($this->state->get('cas_account_link_test.invalidate', FALSE)) {
      $form_state = $event->getFormState();
      $form_state->setErrorByName('account_exist', 'The form has been invalidated by an event subscriber.');
    }
  }

}
