<?php

declare(strict_types = 1);

namespace Drupal\Tests\cas_account_link\Functional;

use Drupal\cas\Service\CasUserManager;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\cas\Traits\CasTestTrait;

/**
 * Tests CAS Account Link module main functionality.
 *
 * @group cas_account_link
 */
class CasAccountLinkTest extends BrowserTestBase {

  use CasTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'cas_account_link_test',
    'cas_mock_server',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->config('cas.settings')
      ->set('user_accounts.auto_register', TRUE)
      ->set('login_success_message', 'Successfully finished')
      ->save();

    $this->createCasUser('beavis', 'beavis@example.com', 'needtp', [
      'firstname' => 'Beavis',
      'lastname' => 'El Gran Cornholio',
    ]);

    // Place the login/logout block so that we can check if user is logged in.
    $this->placeBlock('system_menu_block:account');
  }

  /**
   * Test the case when the user doesn't exist locally.
   */
  public function testNonExistingUser(): void {
    // Login with CAS.
    $this->loginWithCas('user');

    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();

    // The user acknowledges that it doesn't have a local account.
    $page->selectFieldOption('account_exist', 'no');
    $page->pressButton('Continue');

    /** @var \Drupal\user\UserInterface $account */
    $account = user_load_by_name('beavis');
    // Check that the new account has been created and the user is logged in.
    $this->assertNotNull($account);
    $assert_session->pageTextContains('beavis');

    // Check that the user has been redirected.
    $this->assertSession()->statusCodeEquals(200);
    $this->assertEquals(Url::fromRoute('entity.user.canonical', [
      'user' => $account->id(),
    ])->setAbsolute()->toString(), $this->getSession()->getCurrentUrl());
    $this->assertSession()->pageTextContains('Successfully finished');

    // Log out.
    $page->clickLink('Log out');

    // Switch to use a success message and redirect provided by a 3rd-party.
    \Drupal::state()->set('cas_account_link_test.use_custom', TRUE);
    $account->delete();

    // Redo the whole process to check the 3rd-party module integration.
    $this->loginWithCas('user');

    // The user acknowledges that it doesn't have a local account.
    $page = $this->getSession()->getPage();
    $page->selectFieldOption('account_exist', 'no');
    $page->pressButton('Continue');

    // Check that the new account has been created and the user is logged in.
    $account = user_load_by_name('beavis');
    $this->assertNotNull($account);
    $assert_session->pageTextContains('beavis');

    // Check that the user has been redirected.
    $this->assertSession()->statusCodeEquals(200);
    $assert_session->addressEquals($account->toUrl('edit-form')->setAbsolute()->toString());
    $this->assertSession()->pageTextContains('Custom success message');
  }

  /**
   * Tests the case when attempting to register a username that already exists.
   */
  public function testUsernameConflict(): void {
    $this->createUser([], 'beavis');

    // Login with CAS.
    $this->loginWithCas('user');

    // The user selects to add a new account.
    $page = $this->getSession()->getPage();
    $page->selectFieldOption('account_exist', 'no');
    $page->pressButton('Continue');
    $this->assertSession()->pageTextContains('Successfully finished');

    /** @var \Drupal\externalauth\ExternalAuth $external_auth */
    $external_auth = \Drupal::service('externalauth.externalauth');

    // Check that the CAS account has been mapped to an account with a username
    // that starts with the 'beavis' username but has a hash suffix.
    $account = $external_auth->load('beavis', 'cas');
    $this->assertStringStartsWith('beavis_', $account->getAccountName());
    $this->assertEquals(13, mb_strlen($account->getAccountName()));
  }

  /**
   * Test the case when the user exists locally.
   */
  public function testExistingUser(): void {
    $state = \Drupal::state();
    $local_account = $this->createUser([], 'beavis_from_highland');

    // Login with CAS.
    $this->loginWithCas('user');

    // The user acknowledges that it has a local account.
    $page = $this->getSession()->getPage();
    $page->selectFieldOption('account_exist', 'yes');

    // Check a non-existing user name.
    $page->fillField('Username', 'non_existing_user');
    $page->fillField('Password', $this->randomString());
    $page->pressButton('Log in');
    $assert_session = $this->assertSession();
    $assert_session->pageTextContains('Unrecognized username or password. Forgot your password?');

    // Fill in the correct credentials but test form invalidation by a third
    // party event subscriber.
    $state->set('cas_account_link_test.invalidate', TRUE);
    $page->fillField('Username', 'beavis_from_highland');
    $page->fillField('Password', $local_account->pass_raw);
    $page->pressButton('Log in');
    $assert_session->pageTextContains('The form has been invalidated by an event subscriber.');
    $state->delete('cas_account_link_test.invalidate');

    // Fill in the correct credentials.
    $page->fillField('Username', 'beavis_from_highland');
    $page->fillField('Password', $local_account->pass_raw);
    $page->pressButton('Log in');

    // Check that the user has been redirected even the page doesn't exist.
    $this->assertSession()->statusCodeEquals(200);
    $expected_url = $local_account->toUrl()->setAbsolute()->toString();
    $this->assertSame($expected_url, $this->getSession()->getCurrentUrl());
    $assert_session->pageTextContains('beavis_from_highland');
    $this->assertSession()->pageTextContains('Successfully finished');

    /** @var \Drupal\externalauth\AuthmapInterface $authmap */
    $authmap = \Drupal::service('externalauth.authmap');
    // Check that the CAS and local accounts are paired.
    $authname = $authmap->get((int) $local_account->id(), 'cas');
    $this->assertSame('beavis', $authname);

    // Log out.
    $page->clickLink('Log out');

    // Switch to use a success message and redirect provided by a 3rd-party.
    $state->set('cas_account_link_test.use_custom', TRUE);
    $authmap->delete((int) $local_account->id());

    // Redo the whole process to check the 3rd-party module integration.
    $this->loginWithCas('arbitrary-drupal-path');

    // The user acknowledges that it has a local account.
    $page = $this->getSession()->getPage();
    $page->selectFieldOption('account_exist', 'yes');

    $page->fillField('Username', 'beavis_from_highland');
    $page->fillField('Password', $local_account->pass_raw);
    $page->pressButton('Log in');

    // Check that the user has been redirected.
    $this->assertSession()->statusCodeEquals(200);
    $assert_session->addressEquals($local_account->toUrl('edit-form')->setAbsolute()->toString());
    $this->assertSession()->pageTextContains('Custom success message');

    // Check that the CAS and local accounts are paired.
    $authname = $authmap->get((int) $local_account->id(), 'cas');
    $this->assertSame('beavis', $authname);
  }

  /**
   * Tests the access to the CAS Account Link form.
   */
  public function testFormAccess(): void {
    $path = $this->config('cas_account_link.settings')->get('form.path');
    $cas_settings = $this->config('cas.settings');

    // Anonymous, not logged in with CAS, cannot access the form.
    $cas_settings->set('user_accounts.auto_register', FALSE)->save();
    $this->drupalGet($path);
    $assert_session = $this->assertSession();
    $session = $this->getSession();
    $assert_session->statusCodeEquals(403);
    // Enable user_accounts.auto_register.
    $cas_settings->set('user_accounts.auto_register', TRUE)->save();
    $session->reload();
    $assert_session->statusCodeEquals(403);

    // Anonymous user but logged in with CAS (i.e. has CAS context).
    $cas_settings->set('user_accounts.auto_register', FALSE)->save();
    $this->loginWithCas('beavis');
    $assert_session->pageTextContains('You do not have an account on this website. Please contact a site administrator.');
    // Enable user_accounts.auto_register.
    $cas_settings->set('user_accounts.auto_register', TRUE)->save();
    $this->loginWithCas('beavis');
    $session->reload();
    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextContains('Already a user?');

    // Authenticated user.
    $cas_settings->set('user_accounts.auto_register', FALSE)->save();
    $this->drupalLogin($this->createUser());
    $this->drupalGet($path);
    $assert_session->statusCodeEquals(403);
    // Enable user_accounts.auto_register.
    $cas_settings->set('user_accounts.auto_register', TRUE)->save();
    $session->reload();
    $assert_session->statusCodeEquals(403);
  }

  /**
   * Tests a race condition.
   *
   * After a successful CAS login, the user is filling the login form to link
   * its account but, in the meantime, other process is doing account linking.
   */
  public function testRaceAccountPair():void {
    $local_account = $this->createUser([], 'beavis_from_highland');

    // Login with CAS.
    $this->loginWithCas('arbitrary-drupal-path');

    // The user acknowledges that it has a local account.
    $page = $this->getSession()->getPage();
    $page->selectFieldOption('account_exist', 'yes');

    // Fill in the login credentials.
    $page->fillField('Username', 'beavis_from_highland');
    $page->fillField('Password', $local_account->pass_raw);

    // In the meantime, just before the user hits 'Log in', other process is
    // stealing the show, by pairing the accounts. It could be the user itself
    // in other browser session or other arbitrary API/UI process.
    \Drupal::service('externalauth.externalauth')->linkExistingAccount('beavis', 'cas', $local_account);

    // Finally, the user submits the form.
    $page->pressButton('Log in');

    // Check that the user has been redirected even the page doesn't exist.
    $this->assertSession()->statusCodeEquals(404);
    $expected_url = Url::fromUserInput('/arbitrary-drupal-path')->setAbsolute()->toString();
    $this->assertSession()->addressEquals($expected_url);
  }

  /**
   * Test the case when the user doesn't exist locally but their Email does.
   */
  public function testNewUserWithExistingMail(): void {
    // Users are registered with the CAS Email.
    $this->config('cas.settings')
      ->set('user_accounts.email_assignment_strategy', CasUserManager::EMAIL_ASSIGNMENT_ATTRIBUTE)
      ->set('user_accounts.email_attribute', 'email')
      ->save();

    // Create a local user with the same Email address as the CAS user.
    $this->createUser([], NULL, FALSE, ['mail' => 'beavis@example.com']);

    $this->loginWithCas('user');

    // The user acknowledges that it doesn't have a local account.
    $page = $this->getSession()->getPage();
    $page->selectFieldOption('account_exist', 'no');
    $page->pressButton('Continue');

    $this->assertSession()->pageTextContains('The email address beavis@example.com is already taken.');
  }

  /**
   * Logs-in a user to the CAS mock server.
   *
   * @param string|null $destination
   *   A Drupal internal path where the user should return.
   */
  protected function loginWithCas(?string $destination = NULL): void {
    $query = $destination ? ['destination' => $destination] : [];
    $this->casLogin('beavis@example.com', 'needtp', $query);
  }

}
