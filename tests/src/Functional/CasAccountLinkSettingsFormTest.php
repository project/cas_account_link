<?php

declare(strict_types = 1);

namespace Drupal\Tests\cas_account_link\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the CAS Account Link settings form.
 *
 * @group cas_account_link
 */
class CasAccountLinkSettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'cas_account_link',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests the CAS Account Link settings form.
   */
  public function testSettingsForm(): void {
    $this->drupalLogin($this->createUser(['administer site configuration']));

    $this->drupalGet('/cas/account-link');
    $assert_session = $this->assertSession();

    // The page is not accessible but exists, otherwise it would have been 404.
    $assert_session->statusCodeEquals(403);

    $this->drupalGet('/admin/config/people/cas/account-link');

    $assert_session->fieldValueEquals('The path of the form page', '/cas/account-link');
    $assert_session->fieldValueEquals('The title of the form page', 'Already a user?');
    $assert_session->fieldValueEquals('Optional help text to be displayed on top of the form', 'In case you already have a local account, you can link your CAS and your local account using this form.');
    $assert_session->fieldValueEquals('The question text', 'Do you already have an account on this site?');
    $assert_session->fieldValueEquals('The question description', 'If yes, you will be asked to login with your site credentials.');
    $assert_session->fieldValueEquals('The answer label if the account exists', 'Yes');
    $assert_session->fieldValueEquals('The answer description if the account exists', 'You will be asked to login with your site credentials.');
    $assert_session->fieldValueEquals("The form submit button label if the account exists", 'Log in');
    $assert_session->fieldValueEquals("The answer label if the account doesn't exist", 'No');
    $assert_session->fieldValueEquals("The answer description if the account doesn't exist", 'A new account will be created.');
    $assert_session->fieldValueEquals("The form submit button label if the account doesn't exist", 'Continue');

    $page = $this->getSession()->getPage();

    $page->fillField('The path of the form page', '/arbitrary/path');
    $page->fillField('The title of the form page', 'changed Already a user?');
    $page->fillField('Optional help text to be displayed on top of the form', 'changed In case you already have a local account, you can link your CAS and your local account using this form.');
    $page->fillField('The question text', 'changed Do you already have an account on this site?');
    $page->fillField('The question description', 'changed If yes, you will be asked to login with your site credentials.');
    $page->fillField('The answer label if the account exists', 'changed Yes');
    $page->fillField('The answer description if the account exists', 'changed You will be asked to login with your site credentials.');
    $page->fillField('The form submit button label if the account exists', 'changed Log in');
    $page->fillField("The answer label if the account doesn't exist", 'changed No');
    $page->fillField("The answer description if the account doesn't exist", 'changed A new account will be created.');
    $page->fillField("The form submit button label if the account doesn't exist", 'changed Continue');
    $page->pressButton('Save configuration');

    $assert_session->pageTextContains('The configuration options have been saved.');

    // Check that the configuration values were changed.
    $assert_session->fieldValueEquals('The path of the form page', '/arbitrary/path');
    $assert_session->fieldValueEquals('The title of the form page', 'changed Already a user?');
    $assert_session->fieldValueEquals('Optional help text to be displayed on top of the form', 'changed In case you already have a local account, you can link your CAS and your local account using this form.');
    $assert_session->fieldValueEquals('The question text', 'changed Do you already have an account on this site?');
    $assert_session->fieldValueEquals('The question description', 'changed If yes, you will be asked to login with your site credentials.');
    $assert_session->fieldValueEquals('The answer label if the account exists', 'changed Yes');
    $assert_session->fieldValueEquals('The answer description if the account exists', 'changed You will be asked to login with your site credentials.');
    $assert_session->fieldValueEquals("The form submit button label if the account exists", 'changed Log in');
    $assert_session->fieldValueEquals("The answer label if the account doesn't exist", 'changed No');
    $assert_session->fieldValueEquals("The answer description if the account doesn't exist", 'changed A new account will be created.');
    $assert_session->fieldValueEquals("The form submit button label if the account doesn't exist", 'changed Continue');

    $this->drupalGet('/cas/account-link');
    $assert_session = $this->assertSession();

    // Check that the form route was changed.
    $this->drupalGet('/cas/account-link');
    $assert_session->statusCodeEquals(404);
    $this->drupalGet('/arbitrary/path');
    $assert_session->statusCodeEquals(403);
  }

}
